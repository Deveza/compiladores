package Compiladores.Lexico;

public class Imprimir {
	
	public Imprimir(Integer entrada, String lexeme, String identificador, String codigo, String tipo,
		Integer antes_truncagem, Integer dps_truncagem, Integer linhas) {
		
		System.out.println("Entrada: "+entrada + " - Lexeme: " +lexeme+" - Identificador: " +identificador+" - Código: " +codigo+" - Tipo: " +tipo+" - Antes da Truncagem: " +antes_truncagem+" - Depois da Truncagem: " + dps_truncagem + " - Linha: "+linhas);

	}
	
	public Imprimir(String lexeme, String identificador) {
		System.out.println(lexeme+" - Caracterizado por: " +identificador);
	}
	
}