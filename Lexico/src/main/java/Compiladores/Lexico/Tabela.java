package Compiladores.Lexico;

public class Tabela {

	
	public static Integer entrada = 0;
	public String lexeme;
	public String identificador;
	public String codigo;
	public String tipo;
	public Integer antes_truncagem;
	public Integer dps_truncagem;
	public Integer linhas;
	
	public Tabela() {

	}
	public Tabela(String lexeme, String identificador, String codigo, String tipo,
			Integer antes_truncagem, Integer dps_truncagem, Integer linhas) {
		
		entrada++;
		this.lexeme = lexeme;
		this.identificador = identificador;
		this.codigo = codigo;
		this.tipo = tipo;
		this.antes_truncagem = antes_truncagem;
		this.dps_truncagem = dps_truncagem;
		this.linhas = linhas;
	}
	
	public static Integer getEntrada() {
		return entrada;
	}
	public String getLexeme() {
		return lexeme;
	}
	public void setLexeme(String lexeme) {
		this.lexeme = lexeme;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Integer getAntes_truncagem() {
		return antes_truncagem;
	}
	public void setAntes_truncagem(Integer antes_truncagem) {
		this.antes_truncagem = antes_truncagem;
	}
	public Integer getDps_truncagem() {
		return dps_truncagem;
	}
	public void setDps_truncagem(Integer dps_truncagem) {
		this.dps_truncagem = dps_truncagem;
	}
	public Integer getLinhas() {
		return linhas;
	}
	public void setLinhas(Integer linhas) {
		this.linhas = linhas;
	}
	
	
}
