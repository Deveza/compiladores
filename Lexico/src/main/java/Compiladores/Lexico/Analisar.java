package Compiladores.Lexico;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JOptionPane;

public class Analisar {

	public static void main(String[] args) throws IOException {

		String nome = JOptionPane.showInputDialog(null, "Entre com o nome do arquivo");
		String subPath = "src\\main\\arquivos\\";
		String extensao = nome.substring(nome.length() - 3, nome.length());
		BufferedReader br;

		try {
			if (extensao.equals("182")) {
				if (nome.toString().contains(":/")) {
					br = new BufferedReader(new FileReader(nome));
				}else {
					br = new BufferedReader(new FileReader(subPath + nome));
				}
				Object opcoes[] = {"Analisador Léxico","Tabela de Símbolos"};
				int op = JOptionPane.showOptionDialog(null,"Escolha um","Qual deseja?",1,3, null, opcoes, null);
				Lexico lexico = new Lexico(br);
				Tabela tabela;
				if (op == 1) {
					System.out.println("----- Tabela de Símbolos -----");
					while ((tabela = lexico.yylex()) != null) {
						new Imprimir(Tabela.getEntrada(), tabela.getLexeme(), tabela.getIdentificador(), tabela.getCodigo(), tabela.getTipo(), tabela.getAntes_truncagem(), tabela.getDps_truncagem(), tabela.getLinhas());
					}
				}else{
					System.out.println("----- Analisador Léxico -----");
					while ((tabela = lexico.yylex()) != null) {
						new Imprimir(tabela.getLexeme(), tabela.getIdentificador());
					}
				}
				br.close();
			}
			else {
				JOptionPane.showMessageDialog(null, "Formato não Permitido");
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		
	}
}
