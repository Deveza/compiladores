package Compiladores.Lexico;

%%

%public
%class Lexico
%line
%type Tabela

branco = [\n|\s|\t\r]
digito = [0-9]
letra = [a-zA-Z_]
comentario = \|\|*\n
Integer_Number = {digito}+
Identifier = ({letra}|{digito})+({letra}|"_")
Function = {letra}+ | {Identifier} {letra}+ | {Identifier} {digito}+
Constant_String = "\""({letra}+|{branco}+|{digito}+|"$" | "_" | ".")+"\""
Character = "'"{letra}"'"
Float_Number = {digito}+"."{digito}+({exponent_part}{digito}+)?
exponent_part = "e"|"e+"|"e-"

%%
<YYINITIAL> {
"program" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Palavra Reservada", "A12", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"begin" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Palavra Reservada", "A16", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"end" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Palavra Reservada", "A09", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"int" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Palavra Reservada", "A14", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"float" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Palavra Reservada", "A13", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"string" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Palavra Reservada", "A08", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"void" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Palavra Reservada", "A04", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"bool" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Palavra Reservada", "A01", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"char" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Palavra Reservada", "A05", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"true"    { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Palavra Reservada", "A06", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"false"    { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Palavra Reservada", "A11", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"break"    { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Palavra Reservada", "A03", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"if" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Palavra Reservada", "A15", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"else" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Palavra Reservada", "A07", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"while" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Palavra Reservada", "A02", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"return" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Palavra Reservada", "B22", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }

"(" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B03", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
")" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B14", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"[" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B06", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"]" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B17", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"{" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B07", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"}" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B19", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
";" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B05", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"," { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "BB16", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"|" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B18", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"&"  { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B02", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"!"    { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B12", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"="     { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B20", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
">"    { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B22", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
">="    { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B11", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"<"    { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B20", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"<="    { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B09", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"=="    { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B21", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"!="     { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B01", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"+"    { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B08", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"-"    { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B23", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"*"    { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B15", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"/"    { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B04", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"#" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B01", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"%" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Símbolo Reservado", "B13", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }

"Identifier" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Identificador", "C05", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"Integer-Number" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Identificador", "C06", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"Float-Number"    { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Identificador", "C03", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"Constant-String"    { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Identificador", "C02", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"Character"    { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Identificador", "C01", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
"Function" { return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Identificador", "C04", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }

{Identifier}  {return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Identifier", "C05", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
{Function}  {return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Function", "C04", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
{Integer_Number}  {return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Integer_Number", "C06", "IN", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
{Float_Number} {return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Float_Number", "C03", "FL", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
{Character} {return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Character", "C01", "CH", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
{Constant_String} {return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Constant_String", "C02", "ST", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1)); }
{exponent_part} {return new Tabela(yytext().length()> 35?yytext().substring(0, 35):yytext(), "Float_Number", "C03", "FL", yytext().length(), yytext().length()> 35?yytext().substring(0, 35).length():yytext().length(), (yyline+1));}

{comentario} {}
{branco} {}

}

<<EOF>> {}

[^] {  }
